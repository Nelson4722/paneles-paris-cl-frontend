import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from 'views/App';
import '../scss/app.scss';
import 'babel-polyfill';
import es6Promise from 'es6-promise';
es6Promise.polyfill();

ReactDOM.render(
  <AppContainer>
    <App />
  </AppContainer>,
    document.getElementById('app')
);


if (module.hot) {
  module.hot.accept('./views/App/', () => {
    const NewClient = require('./views/App/index').default; // eslint-disable-line global-require

    render(NewClient);
  });
}

