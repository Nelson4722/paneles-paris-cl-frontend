import React from 'react';
import ReactDOMServer from 'react-dom/server';
import express from 'express';

import 'babel-polyfill';

import getServerHtml from 'config/server-html';
import App from 'views/App';

// Load SCSS
import '../scss/app.scss';

const app = express();
const hostname = 'localhost';
const port = 8080;


app.use('/client', express.static('build/client'))


app.use('/', (req, res, next) => {

  const appHtml = ReactDOMServer.renderToString(
    <App />
  );

  const serverHtml = getServerHtml(appHtml);


  if(req.originalUrl === '/')
    // We're good, send the response
	res.status(200).send(serverHtml)
  else{
	next()
  }
  // TODO how to handle 50x errors?
})



// Start listening
app.listen(port, (error) => {
  if (error) {
    console.error(error); // eslint-disable-line
  } else {
    console.info(`\n★★ Listening on port ${ port }. Open up http://${ hostname }:${ port }/ in your browser.\n`); // eslint-disable-line
  }
});
