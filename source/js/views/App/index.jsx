import React, { Component } from 'react';
import ReactTable from 'react-table';
import { Overlay, Spinner } from '@blueprintjs/core';
import { DateRangeInput } from '@blueprintjs/datetime';
import moment from 'moment';
import 'fetch-everywhere';
import NumberFormat from 'react-number-format';
import logoCenco from '../../../assets/img/cenco-logo.png';
import Select from 'react-select'
import MomentLocaleUtils from 'react-day-picker/moment'
import 'moment/locale/es'
import ReactImageMagnify from 'react-image-magnify'

const fecha = new Date()
const mes   = fecha.getMonth()
const año   = fecha.getFullYear()
const dia   = fecha.getDate()

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      locale: 'es',
      data: [],
      title: "Seleccione fecha válida y división para actualizar",
      dateStarting: new Date(año, mes, dia-3),
      dateEnding: new Date(año, mes, dia-1),
      MAX: new Date(año, mes, dia-1),
      MIN: new Date(año, mes, dia-20),
      loading: false,
      validDates: true,
      validSelect:true,
      // dimensiones
      showFecha: true,
      showImg: true,
      showSku: true,
      showDesc: true,
      showMarca: false,
      showDepto: true,
      showSubdepto: false,
      showClase: false,
      // metricas
      showVenta: true,
      showViews: true,
      showConversion: true,
      showUnidades: true,
      showUnidadesV: true,
      showUnidadesVT:true,
      showPrecioNormal: true,
      showPrecioOferta: true,
      showPrecioTarjeta: true,
      showVD: true,
      showB200:true,
      showB12:true,
      showBopis: true,
      showPrecioVentaPromedio: true,
      showPenetracionAcum:true,
      showMargen1:true,
      showRetail:true,
      showContribrucion:false,
      selectedOption: '',
      selectedOrder: '',
      //other stuff
      modifiedSelect: false,
      modifiedOrder: false,
      modifiedDate: false
    };
  }


  handleChangeSelect = (selectedOption) => {
    console.log(selectedOption)
    this.setState({selectedOption: selectedOption, modifiedSelect: true});
  }

  handleChangeOrder = (selectedOrder) => {
    console.log(selectedOrder)
    this.setState({selectedOrder: selectedOrder, modifiedOrder: true});
  }
  changeTitle = () => {
      title: "Actualizado"
   }
  render() {
    const { data } = this.state;

    return (
      <div>
        <Overlay
          isOpen={ this.state.loading }
          canEscapeKeyClose={ false }
          canOutsideClickClose={ false }
        >
        <div className='spinner-container padding'>
          <div className ='padding'>Cargando...</div>
          <div className = 'spinner padding centered'><Spinner/></div>
        </div>
        </Overlay>
        <div className="container well">
            <div id="selector">
              <Select id='form-field-name'
                name="form-field-name"
                onChange={this.handleChangeSelect}
                value={this.state.selectedOption}
                multi={false}
                Select-clear-zone={false}
                style={{width:155}}
                placeholder="Seleccione División"
                wrapperStyle={{width:155}}
                menuContainerStyle={{width:155,height:800}}
                menuStyle={{width:155,height:800}}
                options={[
                  { value: 'DECO-HOGAR', label: 'Deco Hogar' },
                  { value: 'ELECTRO-HOGAR', label: 'Electro Hogar' },
                  { value: 'TECNOLOGIA', label: 'Tecnología' },
                  { value: 'ACCESORIOS', label: 'Accesorios' },
                  { value: 'DEPORTES', label: 'Deportes' },
                  { value: 'HOMBRES', label: 'Hombres' },
                  { value: 'INFANTIL', label: 'Infantil' },
                  { value: 'MUJER', label: 'Mujer' },
                  ]}
                />
            </div>
            <div id="calendario">
              &nbsp; Desde:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Hasta:
            </div>
            <div id="calendario">
              <DateRangeInput
                localeUtils={MomentLocaleUtils}
                locale={this.state.locale}
                value={[this.state.dateStarting, this.state.dateEnding]}
                onChange={this.handleDateChange}
                maxDate={this.state.MAX}
                minDate={this.state.MIN}
                format="DD-MM-YYYY"
                defaultValue={this.state.MIN,this.state.MAX}
              />&nbsp; &nbsp;
              <button className = "btn"
                disabled={!(this.state.modifiedDate || this.state.modifiedSelect)}
                title='Seleccione fecha válida y división para actualizar'
                onClick = {this.getData}>
                <span className='glyphicon glyphicon-refresh'><span>Actualizar</span></span>
              </button>
            </div>
            <div id='dimensiones'>
          <p>Dimensiones</p>
            <span> Marca:
                <input type='checkbox' onChange={this.handleToggle} name='showMarca' checked={this.state.showMarca} />
            </span>
            <span>  SubDepto:
                <input type='checkbox' onChange={this.handleToggle} name='showSubdepto' checked={this.state.showSubdepto} />
            </span>
            <span>  Clase:
                <input type='checkbox' onChange={this.handleToggle} name='showClase' checked={this.state.showClase} />
            </span>
        </div>
        <div id='metricas'>
          <p>Métricas</p>
            <span> Precio Tarjeta:
                <input type='checkbox' onChange={this.handleToggle} name='showPrecioTarjeta' checked={this.state.showPrecioTarjeta} />
            </span>
            <span> Stock VD:
                <input type='checkbox' onChange={this.handleToggle} name='showVDs' checked={this.state.showBopis} />
            </span>
            <span> Stock Bopis:
                <input type='checkbox' onChange={this.handleToggle} name='showBopis' checked={this.state.showBopis} />
            </span>
            <span> Penetración T:
                <input type='checkbox' onChange={this.handleToggle} name='showPenetracionAcum' checked={this.state.showPenetracionAcum} />
            </span>
            <span> Contribución:
                <input type='checkbox' onChange={this.handleToggle} name='showContribrucion' checked={this.state.showContribrucion} />
            </span>
        </div>

          </div>

        <ReactTable
          data={ data }
          resizable={ true }
          noDataText={ this.state.loading ?  'Cargando' : 'No hay data'  }
          columns={[
            {
              Header: <div><span className={'bold'}><p>Dimensiones</p></span></div>,
              columns: [
                {
                  Header: <div><span className={'bold'}><p>Imagen</p><p>Descripción</p></span></div>,
                  id: 'img_prod_1',
                  show: this.state.showImg,
                  accessor: 'descripcion',
                  width: 87,
                  filterable: true,
                  filterMethod: (filter, row) => {return row[filter.id].toString().toLowerCase().includes(filter.value.toLowerCase()) },
                  Cell: row => (
                    <div>
                    <a href={row.original.url} target="_blank"><img className='thumbnail' src={row.original.img_prod_1} /></a>
                    <spam className='desc'>{row.original.descripcion}</spam>
                    </div>
                  )
                },
                {
                  Header: <div><span className={'bold'}><p>Estilo</p></span></div>,
                  id: 'sku',
                  filterMethod: (filter, row) =>{return row[filter.id].toString().includes(filter.value)},
                  filterable: true,
                  show: this.state.showSku,
                  Cell: row => (
                    <div>
                    <br/>
                      <span className={'bold'}>{row.original.sku}</span>
                    </div>
                  ),
                  maxWidth: 52,
                  accessor: 'sku'
                },
                {
                  Header: <div><span className={'bold'}><p>Marca</p></span></div>,
                  id: 'marca',
                  filterable: true,
                  filterMethod: (filter, row) => { return row[filter.id].toString().toLowerCase().includes(filter.value.toLowerCase()) },
                  show: this.state.showMarca,
                  Cell: row => (
                    <div>
                    <br/>
                      <span className={'bold'}>{row.original.marca}</span>
                    </div>
                  ),
                  maxWidth: 58,
                  accessor: 'marca'
                },
                {
                  Header: <div><span className={'bold'}><p>Depto</p></span></div>,
                  id: 'depto',
                  filterable: true,
                  filterMethod: (filter, row) => { return row[filter.id].toString().includes(filter.value) },
                  show: this.state.showDepto,
                  Cell: row => (
                    <div>
                    <br/>
                      <span className={'bold'}>{row.original.depto}</span>
                    </div>
                  ),
                  maxWidth: 51,
                  accessor: 'depto'
                },
                {
                  Header: <div><span className={'bold'}><p>Sub</p><p>Depto</p></span></div>,
                  id: 'subdepto',
                  filterable: true,
                  filterMethod: (filter, row) => { return row[filter.id].toString().includes(filter.value) },
                  show: this.state.showSubdepto,
                  Cell: row => (
                    <div>
                    <br/>
                      <span className={'bold'}>{row.original.subdepto}</span>
                    </div>
                  ),
                  maxWidth: 49,
                  accessor: 'subdepto'
                },
                {
                  Header: <div><span className={'bold'}><p>Clase</p></span></div>,
                  id: 'clase',
                  filterable: true,
                  filterMethod: (filter, row) => { return row[filter.id].toString().includes(filter.value) },
                  show: this.state.showClase,
                  Cell: row => (
                    <div>
                    <br/>
                      <span className={'bold'}>{row.original.clase}</span>
                    </div>
                  ),
                  maxWidth: 75,
                  accessor: 'clase'
                }
              ]
            },
            {
              Header: <div><span className={'bold'}><p>Métricas</p></span></div>,
              columns: [
                {
                  Header: <div><span className={'bold'}><p>Fecha</p></span></div>,
                  id: 'fechacomp',
                  width: 127,
                  accessor: 'venta_neta',
                  show: this.state.showVenta,
                  Cell: row => (
                    <div>
                    <br/>

                      <p> <span className={'bold'}>
                        <span>{moment().format('DD-MM-YYYY')} </span>
                              </span>
                      </p>

                      <p> <span className={'bold'}>
                        <span> {moment(this.state.dateStarting).format('DD-MM-YY')}</span> a
                        <span> {moment(this.state.dateEnding).format('DD-MM-YY')}</span><br/>
                              </span>
                      </p>

                    </div>
                  )
                },
                {
                  Header: <div id='cabecera'><span title='Venta Bruta'><span className={'bold'}><p>Venta</p></span></span></div>,
                  id: 'venta_neta'+'listOrder',
                  maxWidth: 82,
                  accessor: 'venta_neta',
                  show: this.state.showVenta,
                  Cell: row => (
                    <div>

                    <p> <span className={'bold'}>
                    <br/>
                        <NumberFormat value={ row.original.venta_neta } displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'}  />
                              </span>
                      </p>
                           <p> <span className={'bold'}>
                        <NumberFormat value={ row.original.venta_neta_acum } displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                              </span>
                      </p>

                    </div>
                  )
                },
                {
                 Header: <div><span title='Unidades Vendidas'><span className={'bold'}><p>Unidades</p></span></span></div>,
                 id: 'unidades',
                 filterable: true,
                 filterMethod: (filter, row) => { return row[filter.id]>=(filter.value)},
                 accessor: 'unidades',
                 maxWidth: 69,
                 show: this.state.showUnidadesV,
                 Cell: row => (
                   <div>

                    <p> <span className={'bold'}>
                    <br/>
                        <NumberFormat value={ row.original.unidades } displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                              </span>
                      </p>
                           <p> <span className={'bold'}>
                        <NumberFormat value={ row.original.unidades_acum } displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                              </span>
                      </p>


                   </div>
                 )
               },
               {
                 Header: <div><span title='Unidades Vendidas con Tarjeta Cencosud'><span className={'bold'}><p>Unidades</p><p>T</p></span></span></div>,
                 id: 'unidadesVT',
                 filterable: true,
                 filterMethod: (filter, row) => { return row[filter.id]>=(filter.value)},
                 accessor: 'unidadesVT',
                 maxWidth: 69,
                 show: this.state.showUnidadesVT,
                 Cell: row => (
                   <div>

                     <p> <span className={'bold'}>
                    <br/>
                        <NumberFormat value={ row.original.unidades_tarjeta } displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                      </span>
                      </p>
                           <p> <span className={'bold'}>
                        <NumberFormat value={ row.original.unidades_tarjeta_acum } displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                       </span>
                      </p>
                   </div>
                 )
                },
                {
                  Header: <div><span title='Número de visitas del producto'><span className={'bold'}><p>Views</p></span></span></div>,
                  id: 'product_views',
                  filterable: true,
                 filterMethod: (filter, row) => { return row[filter.id]>=(filter.value)},
                  accessor: 'product_views',
                  maxWidth: 48,
                  show: this.state.showViews,
                  Cell: row => (
                    <div>
                    <br/>
                    <p> <span className={'bold'}>
                        <NumberFormat value={ row.original.product_views } displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                              </span>
                      </p>
                           <p> <span className={'bold'}>
                        <NumberFormat value={ row.original.product_views_acum } displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                              </span>
                      </p>

                    </div>
                  )
                },
                {
                  Header: <div><span title="unidades vendidas / views"><span className={'bold'}><p>Conversión</p></span></span></div>,
                  id: 'conversion',
                  accessor: 'conversion',
                  maxWidth: 82,
                  show: this.state.showConversion,
                  Cell: row => (
                    <div>
                        <p><br /> <span className={'bold'}><NumberFormat value={row.original.conversion} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} /> % </span>
                            <span className= { row.original.conversion > row.original.conversion_acum ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down' } style= {{ color: row.original.conversion > row.original.conversion_acum ? '#5cb85c' : '#d9534f' }} ></span></p>
                        <p>
                        <span className={'bold'}><NumberFormat value={row.original.conversion_acum} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} /> % &nbsp; &nbsp; </span>
                        </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Precio Normal Cargado en CMC'><span className={'bold'}><p>Precio</p><p>Normal</p></span></span></div>,
                  id: 'precio_Normal',
                  accessor: 'precio_normal',
                  maxWidth: 74,
                  show: this.state.showPrecioNormal,
                  Cell: row => (
                    <div>
                    <br/>
                        <p><span className={'bold'} style= {{ color:'#0099CC'}}>
                          <NumberFormat value={row.original.precio_normal} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                                </span><br />
                        </p>
                        <p> <span className={'bold'} style= {{ color:'#0099CC'}}>
                          <NumberFormat value={row.original.precio_normal_medio_acum} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                                </span>
                        </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Precio Oferta Cargado en CMC'><span className={'bold'}><p>Precio</p><p>Oferta</p></span></span></div>,
                  id: 'precio_oferta',
                  accessor: 'precio_oferta',
                  maxWidth: 76,
                  show: this.state.showPrecioOferta,
                  Cell: row => (
                    <div>
                    <br/>
                      <p> <span className={'bold'} style= {{ color:'#0099CC'}}>
                        <NumberFormat value={row.original.precio_oferta} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                              </span><br />
                      </p>
                      <p><span className={'bold'} style= {{ color:'#0099CC'}}>
                        <NumberFormat value={row.original.precio_oferta_medio_acum} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                              </span>
                      </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Precio Tarjeta Cargado en CMC'><span className={'bold'}><p>Precio</p><p>Tarjeta</p></span></span></div>,
                  id: 'precio_tarjeta',
                  accessor: 'precio_tarjeta',
                  maxWidth: 74,
                  show: this.state.showPrecioTarjeta,
                  Cell: row => (
                    <div>
                    <br/>
                      <p><span className={'bold'} style= {{ color:'#0099CC'}}>
                        <NumberFormat value={row.original.precio_tarjeta} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                              </span>
                              <br />
                      </p>
                      <p>
                       <span className={'bold'} style= {{ color:'#0099CC'}}>
                        <NumberFormat value={row.original.precio_tarjeta_promedio} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                              </span>
                      </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Precio Promedio pagado por el cliente al comprar'><span className={'bold'}><p>Venta</p><p>Promedio</p></span></span></div>,
                  id: 'precio_venta_promedio',
                  accessor: 'precio_venta_promedio',
                  maxWidth: 71,
                  show: this.state.showPrecioVentaPromedio,
                  Cell: row => (
                    <div>
                    <br/>
                      <p>
                        <spam>
                          <NumberFormat value={ row.original.precio_venta_promedio } displayType={ 'text' } thousandSeparator={'.'} decimalSeparator={','} prefix={ '$' } className={ 'bold' } />
                       </spam>
                       <br />
                      </p>
                      <p><span className={'bold'}>
                        <NumberFormat value={row.original.precio_venta_promedio_acum} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                              </span>

                      </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Stock Vista Disponibilidad'><span className={'bold'}><p>Stock</p><p>VD</p></span></span></div>,
                  id: 'vd',
                  filterable: true,
                  filterMethod: (filter, row) => { return row[filter.id]>=(filter.value)},
                  accessor: 'vd',
                  maxWidth: 49,
                  show: this.state.showVD,
                  Cell: row => (
                    <div>
                    <br/>
                      <p> <span className={'bold'}>
                        <NumberFormat value={row.original.vd} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                              </span>
                      </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Stock Disponible en la Tienda'><span className={'bold'}><p>Stock</p><p>Bopis</p></span></span></div>,
                  id: 'bopis',
                  filterable: true,
                  filterMethod: (filter, row) => { return row[filter.id]>=(filter.value)},
                  accessor: 'bopis',
                  maxWidth: 50,
                  show: this.state.showBopis,
                  Cell: row => (
                    <div>
                    <br/>
                      <p><span className={'bold'}>
                        <NumberFormat value={row.original.bopis} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                              </span>
                      </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Stock en Bodega 200'><span className={'bold'}><p>B</p><p>200</p></span></span></div>,
                  id: 'b200',
                  filterable: true,
                  filterMethod: (filter, row) => { return row[filter.id]>=(filter.value)},
                  accessor: 'b200',
                  maxWidth: 40,
                  show: this.state.showB200,
                  Cell: row => (
                    <div>
                    <br/>
                      <p><span className={'bold'}>
                        <NumberFormat value={row.original.bdocientos} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                              </span>
                      </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Stock en Bodega 12'><span className={'bold'}><p>B</p><p>12</p></span></span></div>,
                  id: 'b12',
                  filterable: true,
                  filterMethod: (filter, row) => { return row[filter.id]>=(filter.value)},
                  accessor: 'b12',
                  maxWidth: 42,
                  show: this.state.showB12,
                  Cell: row => (
                    <div>
                    <br/>
                      <p><span className={'bold'}>
                        <NumberFormat value={row.original.bdoce} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                              </span>
                      </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Penetración con T Cencosud. &#10; Unidades Vendidas con T Cencosud / Unidades'><span className={'bold'}><p>Penetración</p><p>T</p></span></span></div>,
                  id: 'penetracion_acum',
                  accessor: 'penetracion_acum',
                  maxWidth: 85,
                  show: this.state.showPenetracionAcum,
                  Cell: row => (
                    <div>
                        <p><br /> <span className={'bold'}><NumberFormat value={row.original.penetracion} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} /> % </span>
                            <span className= { row.original.penetracion > row.original.penetracion_acum ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down' } style= {{ color: row.original.penetracion > row.original.penetracion_acum ? '#5cb85c' : '#d9534f' }} ></span></p>

                        <p>
                        <span className={'bold'}><NumberFormat value={row.original.penetracion_acum} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} /> % &nbsp; &nbsp; </span>
                        </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Margen'><span className={'bold'}><p>Margen</p></span></span></div>,
                  id: 'margen1',
                  accessor: 'margen1_acum',
                  maxWidth: 72,
                  show: this.state.showMargen1,
                  Cell: row => (
                    <div>
                        <p><br /> <span className={'bold'}><NumberFormat value={row.original.margen1 == null ? 0 : row.original.margen1} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} /> % </span>
                        <span className= { row.original.margen1 > row.original.margen1_acum ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down' } style= {{ color: row.original.margen1 > row.original.margen1_acum ? '#5cb85c' : '#d9534f' }} ></span></p>
                        <p>
                        <span className={'bold'}><NumberFormat value={row.original.margen1_acum == null ? 0 : row.original.margen1_acum} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} /> % &nbsp; &nbsp; </span>
                        </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Contribución'><span className={'bold'}><p>Contribución</p></span></span></div>,
                  id: 'contribucion_pesos',
                  accessor: 'contribucion_pesos_acum',
                  maxWidth: 96,
                  show: this.state.showContribrucion,
                  Cell: row => (
                    <div>
                    <br/>
                     <p><span className={'bold'}>
                        <NumberFormat value={row.original.contribucion_pesos == null ? 0 : row.original.contribucion_pesos} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                              </span>
                      </p>
                      <p><span className={'bold'}>
                      <NumberFormat value={row.original.contribucion_pesos_acum == null ? 0 : row.original.contribucion_pesos_acum} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                            </span>
                    </p>
                    </div>
                  )
                },
                {
                  Header: <div><span title='Competencia'><span className={'bold'}><p>Competencia</p></span></span></div>,
                  id: 'retail',
                  accessor: 'retail',
                  maxWidth: 96,
                  show: this.state.showRetail,
                  Cell: row => (
                    <div>
                    <br/>
                     <p><span className={'bold'}>{row.original.retail}</span>
                      </p>
                      <p><span className={'bold'}>
                      <NumberFormat value={row.original.competencia_precio == null ? 0 : row.original.competencia_precio} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />
                            </span>
                    </p>
                    </div>
                  )
                }
              ]
            },{
              Header: '',
              maxWidth: 1,
              columns: [
                {
                  Header: ''

                }
              ]
            }

          ]}
          showPagination={true}
          defaultPageSize={300}
          style={{
            width: "103%",
            height: "80%" // This will force the table body to overflow and scroll, since there is not enough room
          }}


        />
    </div>
    )
  }



  getData = () => {
      this.setState({ loading: true })
      const start = this.state.dateStarting
      const end = this.state.dateEnding

      const  listValues = this.state.selectedOption.value
      const  listOrder = this.state.selectedOrder.value


      const url = `http://localhost:10010/pariscl?dateStarting=${start}&dateEnding=${end}&listValues=${listValues}`
      console.log(url)

      fetch(url, {
          method: 'GET',
          headers: {
              'Content-Type': 'application/json'
          }
      })
      .then((response) => response.json())
      .then((response) => {
          this.setState({ data: response })
      })
      .catch((response) => {
          console.error('response > ' + response)
      })
      .then(() => {
          this.setState({ loading: false })
      })
  }

  handleToggle = (event) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value
    })
  }

  handleDateChange = (dates) => {
    let start = dates[0] !== null ? dates[0] : new Date(),
          end = dates[1] !== null ? dates[1] : new Date()
    this.validateDates(start, end)
  }

  validateDates = (start, end) => {
    const startFormatted = moment(start).format('YYYYMMDD')
    const endFormatted = moment(end).format('YYYYMMDD')

    if (start !== null && end !== null && moment(start).isBefore(end)) {
      this.setState({
        dateStarting: startFormatted,
        dateEnding: endFormatted,
        validDates: true
      })
    } else {
      this.setState({validDates: false, modifiedDate: true})
    }
  }

}
